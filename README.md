# Wo kann ich PDF + Anki Deck fertig kompiliert runterladen?
##[Releases](https://git.scc.kit.edu/urpyg/GGdG_Latex_Anki/-/releases)

# How to Compile
## PDF
LuaLatex, nicht PdfTex
## Anki
### Vorbereitung
1\. Erweiterungen hinzufügen (und Anki neu starten)

  * [Latex Note importer](https://tentativeconvert.github.io/LaTeX-Note-Importer-for-Anki/)
  * [Edit LaTeX build process](https://ankiweb.net/shared/info/937148547)
  
2\.  Extras > Notiztypen Verwalten > Basic > Einstellungen... 

   * [x] Skalierbare Vektorgrafiken mit dvisgm erzeugen
   
   * Kopfzeile: 
    <pre><code>\documentclass[12pt]{article}
\special{papersize=3in,5in}
\usepackage[utf8]{luainputenc}
\usepackage{amssymb,amsmath}
\usepackage[dvipsnames]{xcolor}
\RequirePackage{lmodern}
\RequirePackage{microtype}
\RequirePackage{tabularx}
\RequirePackage{graphicx}
\usepackage[thmmarks,amsmath]{ntheorem}
\pagestyle{empty}
\setlength{\parindent}{0in}
\newcommand{\detail}[1]{{\scriptsize(#1)\par}~}
\newcommand{\refs}[1]{{\scriptsize\textit{Refs: }#1\par}\hfill.}
\newcommand*{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\N}{{\ensuremath{\mathbb{N}}}}
\newcommand{\R}{{\ensuremath{\mathbb{R}}}}
\newcommand{\Z}{{\ensuremath{\mathbb{Z}}}}
\newcommand{\C}{{\ensuremath{\mathbb{C}}}}
\newcommand{\Q}{{\ensuremath{\mathbb{Q}}}}
\newcommand{\cP}{\ensuremath{\mathcal{P}}}
\newcommand{\A}{\ensuremath{\mathcal{A}}}
\newcommand{\E}{\ensuremath{\mathcal{E}}}
\newcommand{\cNP}{\ensuremath{\mathcal{NP}}}
\newcommand*{\units}{\times}
\usepackage{accents}
\newcommand*{\dt}[1]{%
\accentset{\mbox{\large\bfseries .}}{#1}}
\theoremstyle{nonumberplain}
\newtheorem{folgerung}{Folgerung}
%\usepackage{bbm}
\usepackage{mathtools}
\usepackage{xfrac}
\DeclareMathAlphabet{\mymathbb}{U}{bbold}{m}{n}
\DeclareMathAlphabet{\mathbbm}{U}{bbold}{m}{n}
\DeclareMathAlphabet{\nsmathbb}{U}{bbold}{m}{n}
\usepackage{csquotes}
\usepackage{tikz}
\definecolor{cfff}{RGB}{255,255,255}
\newcommand{\globalscale}{1.000000}
\newcommand{\absolutePngPath}{/pfad/zu/den/bildern}
\usepackage{relsize}
% END OF THE PREAMBLE
\begin{document}
\color{black}
\nopagecolor</code></pre>
   
   * Fußzeile  <pre><code>\end{document}</code></pre>
   
3\. Extras > Erweiterungen > Edit LaTeX build process > Konfiguration > Folgendes Einfügen
<pre><code>{
    "pngCommands": [
        [
            "lualatex",
            "-interaction=nonstopmode",
            "tmp.tex"
        ],
        [
            "convert",
            "-density",
            "200",
            "-trim",
            "tmp.pdf",
            "tmp.png"
        ]
    ],
    "svgCommands": [
        [
            "lualatex",
            "-interaction=nonstopmode",
            "tmp.tex"
        ],
        [
            "pdfcrop",
            "tmp.pdf",
            "tmp.pdf"
        ],
        [
            "pdf2svg",
            "tmp.pdf",
            "tmp.svg"
        ]
    ]
}

</code></pre>

Siehe auch [Guide on using Luatex with Anki](https://unhewn-thoughts.org/guide-on-using-latex-xetex-luatex-with-anki#adjusting-ankis-latex-build-process)

4\.  Eingebettete Tikz Dateien in eine Datei Packen

     latexpand GGdG_Karteikarten.tex > foranki.tex

5\. Datei > Importieren... > foranki.tex Datei auswählen > Öffnen > Importieren

6\. Zum Generieren der SVGs: Extras > Medien Überprüfen > Latex Bilder Generieren